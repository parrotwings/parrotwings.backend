﻿using ParrotWings.Core.Model.Transaction;
using PrimeNG.TableFilter.Models;
using System.Threading.Tasks;

namespace ParrotWings.Core.Services
{
    public interface ITransactionService
    {
        decimal GetBalance(long userId);
        TransactionSearchResult GetTransactions(long userId, TableFilterModel criteria);
        Task<TransactionResponse> CreateTransaction(long senderUserId, TransactionRequest request);
    }
}
