﻿using System;

namespace ParrotWings.Core.Domain
{
    public class AuditableEntity : Entity, IAuditableEntity
    {
        public DateTime CreatedDate { get; set; }
    }
}
