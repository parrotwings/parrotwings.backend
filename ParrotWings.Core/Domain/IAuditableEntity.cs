﻿using System;

namespace ParrotWings.Core.Domain
{
    public interface IAuditableEntity : IEntity
    {
        DateTime CreatedDate { get; set; }
    }
}
