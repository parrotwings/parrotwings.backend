﻿namespace ParrotWings.Core.Model.Account
{
    public class UserRegistration
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
