﻿namespace ParrotWings.Core.Model.Common
{
    public class ApiResult
    {
        public bool Successfully { get; set; }
        public string Text { get; set; }
        public dynamic Object { get; set; }
    }
}
