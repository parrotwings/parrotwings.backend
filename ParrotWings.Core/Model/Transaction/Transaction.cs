﻿using ParrotWings.Core.Domain;

namespace ParrotWings.Core.Model.Transaction
{
    public class Transaction : AuditableEntity
    {
        public long UserId { get; set; }
        public long CorrespondentId { get; set; }
        public string CorrespondentName { get; set; }
        public decimal Amount { get; set; }
    }
}
