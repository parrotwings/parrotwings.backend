﻿namespace ParrotWings.Core.Model.Transaction
{
    public class TransactionRequest
    {
        public string UserEmail { get; set; }
        public decimal Amount { get; set; }
    }
}
