﻿namespace ParrotWings.Core.Model.Transaction
{
    public class TransactionResponse
    {
        public bool Successfully { get; set; }
        public string Text { get; set; }
    }
}
