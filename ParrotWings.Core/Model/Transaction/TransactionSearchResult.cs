﻿using System.Collections.Generic;

namespace ParrotWings.Core.Model.Transaction
{
    public class TransactionSearchResult
    {
        public List<Transaction> Transactions { get; set; }
        public int TotalCount { get; set; }
    }
}
