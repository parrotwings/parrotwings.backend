﻿using FluentValidation;
using ParrotWings.Core.Model.Transaction;

namespace ParrotWings.Web.Validator
{
    public class TransactionRequestValidator : AbstractValidator<TransactionRequest>
    {
        public TransactionRequestValidator()
        {
            RuleFor(vm => vm.UserEmail).NotEmpty().WithMessage("Email cannot be empty");
            RuleFor(vm => vm.Amount).NotEmpty().WithMessage("Amount cannot be empty");
            RuleFor(vm => vm.Amount).GreaterThan(0).WithMessage("Amount should be more 0");
        }
    }
}
