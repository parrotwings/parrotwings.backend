﻿using FluentValidation;
using ParrotWings.Core.Model.Account;

namespace ParrotWings.Web.Validator
{
    public class UserRegistrationValidator : AbstractValidator<UserRegistration>
    {
        public UserRegistrationValidator()
        {
            RuleFor(vm => vm.Email).NotEmpty().WithMessage("Email cannot be empty");
            RuleFor(vm => vm.Password).NotEmpty().WithMessage("Password cannot be empty");
            RuleFor(vm => vm.ConfirmPassword).Equal(vm => vm.Password).WithMessage("Confirm password must match password");
            RuleFor(vm => vm.ConfirmPassword).NotEmpty().WithMessage("Confirm password cannot be empty");
        }
    }
}
