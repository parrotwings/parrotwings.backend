﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace ParrotWings.Web.Helpers
{
    public static class Errors
    {
        public static ModelStateDictionary AddErrorsToModelState(IdentityResult identityResult, ModelStateDictionary modelState)
        {
            foreach (var e in identityResult.Errors)
            {
                modelState.TryAddModelError(e.Code, e.Description);
            }

            return modelState;
        }

        public static ModelStateDictionary AddErrorsToModelState(IList<ValidationFailure> errors, ModelStateDictionary modelState)
        {
            foreach (var e in errors)
            {
                modelState.TryAddModelError(e.ErrorCode, e.ErrorMessage);
            }

            return modelState;
        }
    }
}
