﻿using IdentityModel;
using System.Security.Claims;

namespace ParrotWings.Web.Helpers
{
    public static class Utilities
    {
        public static string GetUserId(ClaimsPrincipal user)
        {
            return user.FindFirst(JwtClaimTypes.Subject)?.Value?.Trim();
        }
    }
}
