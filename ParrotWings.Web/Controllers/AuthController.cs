﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ParrotWings.Core.Model.Account;
using ParrotWings.Core.Model.Common;
using ParrotWings.Infrastructure.Security;
using ParrotWings.Web.Helpers;
using ParrotWings.Web.Validator;
using System.Threading.Tasks;

namespace ParrotWings.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]

    public class AuthController : ControllerBase
    {

        private readonly UserManager<ApplicationUser> _userManager;

        public AuthController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost(Name = "register")]
        public async Task<ActionResult<ApiResult>> RegisterAsync([FromBody] UserRegistration model)
        {
            var userValidator = new UserRegistrationValidator();
            var validatorResult = userValidator.Validate(model);
            if (!validatorResult.IsValid)
            {
                return new ApiResult
                {
                    Successfully = false,
                    Text = "Model is incorrect. See inner exception",
                    Object = Errors.AddErrorsToModelState(validatorResult.Errors, ModelState)
                };
            }

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                return new ApiResult
                {
                    Successfully = false,
                    Text = "User with the same email already exists."
                };
            }

            user = new ApplicationUser { Email = model.Email, UserName = model.Email };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return new ApiResult
                {
                    Successfully = false,
                    Text = "User name or password is incorrect",
                    Object = Errors.AddErrorsToModelState(result, ModelState)
                };
            }
            return new ApiResult
            {
                Successfully = true,
                Text = "Your user has been successfully created."
            };
        }
    }
}
