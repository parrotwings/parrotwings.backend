﻿using Microsoft.AspNetCore.Mvc;
using ParrotWings.Core.Model.Common;
using ParrotWings.Core.Model.Transaction;
using ParrotWings.Core.Services;
using ParrotWings.Web.Helpers;
using ParrotWings.Web.Validator;
using PrimeNG.TableFilter.Models;

namespace ParrotWings.Web.Controllers
{
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    [Produces("application/json")]
    [Route("/api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _transactionService;
        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet("balance", Name = nameof(Balance))]
        public ActionResult<decimal> Balance()
        {
            return Ok(_transactionService.GetBalance(1));
        }

        [HttpPost("search", Name = nameof(Transactions))]
        public ActionResult<TransactionSearchResult> Transactions([FromBody] TableFilterModel criteria)
        {
            var userId = Utilities.GetUserId(User);
            var result = _transactionService.GetTransactions(1, criteria);
            return Ok(result);
        }

        [HttpPost("create", Name = nameof(CreateTransaction))]
        public ActionResult<ApiResult> CreateTransaction([FromBody] TransactionRequest request)
        {
            var transactionValidator = new TransactionRequestValidator();
            var validatorResult = transactionValidator.Validate(request);
            if (!validatorResult.IsValid)
            {
                return new ApiResult
                {
                    Successfully = false,
                    Text = "Model is incorrect. See inner exception",
                    Object = Errors.AddErrorsToModelState(validatorResult.Errors, ModelState)
                };
            }

            var userId = Utilities.GetUserId(User);

            var result = _transactionService.CreateTransaction(1, request);

            return Ok(new ApiResult());
        }

    }
}
