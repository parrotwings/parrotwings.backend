﻿using ParrotWings.Core.Domain;
using ParrotWings.Core.Model.Transaction;

namespace ParrotWings.Infrastructure.Model
{
    public class TransactionEntity : AuditableEntity
    {
        public long UserId { get; set; }
        public long CorrespondentId { get; set; }
        public string CorrespondentName { get; set; }
        public decimal Amount { get; set; }

        public TransactionEntity FromModel(Transaction transaction, bool isOutgoing = true)
        {
            UserId = transaction.UserId;
            CorrespondentId = transaction.CorrespondentId;
            CorrespondentName = transaction.CorrespondentName;
            Amount = isOutgoing ? -transaction.Amount : transaction.Amount;

            return this;
        }
    }
}
