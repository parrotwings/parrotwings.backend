﻿using Microsoft.AspNetCore.Identity;
using ParrotWings.Core.Model.Transaction;
using ParrotWings.Core.Services;
using ParrotWings.Infrastructure.Model;
using ParrotWings.Infrastructure.Repositories.Interface;
using ParrotWings.Infrastructure.Security;
using PrimeNG.TableFilter;
using PrimeNG.TableFilter.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParrotWings.Infrastructure.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly UserManager<ApplicationUser> _userManager;

        public TransactionService(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<TransactionResponse> CreateTransaction(long userId, TransactionRequest request)
        {
            // SemaphoreSlim 
            var recipient = await _userManager.FindByEmailAsync(request.UserEmail);

            if (recipient == null)
            {
                return new TransactionResponse() { Successfully = false, Text = "User with this email was not found" };
            }

            // Check balance
            try
            {
                ExecuteTransactionAsync(userId, recipient, request);

            }
            catch (Exception e)
            {
                // need log
                return new TransactionResponse()
                {
                    Successfully = false,
                    Text = "Sorry, an error has occurred"
                };
            }

            return new TransactionResponse()
            {
                Successfully = true,
                Text = "Successfully"
            };
        }

        public decimal GetBalance(long userId)
        {
            return _unitOfWork.Transactions.CalculateBalance(userId);
        }

        public TransactionSearchResult GetTransactions(long userId, TableFilterModel criteria)
        {
            var totalCount = 0;

            var query = _unitOfWork.Transactions.GetUserTransaction(userId)
                .Select(t => new Transaction()
                {
                    CorrespondentName = t.CorrespondentName,
                    Amount = t.Amount,
                    CreatedDate = t.CreatedDate

                });

            query = query.PrimengTableFilter(criteria, ref totalCount);

            return new TransactionSearchResult()
            {
                TotalCount = totalCount,
                Transactions = query.ToList()
            };
        }

        public void CreateBonusTransaction(long userId)
        {
            var outgoing = new TransactionEntity()
                .FromModel(new Transaction
                {
                    UserId = 1,
                    Amount = 500,
                    CorrespondentId = 1,
                    CorrespondentName = "s"
                });

            _unitOfWork.Transactions.Add(outgoing);

        }

        public void ExecuteTransactionAsync(long senderUserId, ApplicationUser recipient, TransactionRequest request)
        {
            CreateTransaction(senderUserId, recipient, request);

            //using (var transaction = await _applicationDbContext.Database.BeginTransactionAsync())
            //{
            //    try
            //    {
            //        await CreateTransaction(senderUserId, recipient, request);
            //        transaction.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        transaction.Rollback();
            //        throw;
            //    }
            //}
        }

        private void CreateTransaction(long senderUserId, ApplicationUser recipientUser, TransactionRequest request)
        {
            var outgoing = new TransactionEntity()
                    .FromModel(new Transaction
                    {
                        UserId = senderUserId,
                        Amount = request.Amount,
                        CorrespondentId = recipientUser.Id,
                        CorrespondentName = recipientUser.Email
                    });

            _unitOfWork.Transactions.Add(outgoing);

            var incoming = new TransactionEntity()
                .FromModel(new Transaction
                {
                    UserId = recipientUser.Id,
                    Amount = request.Amount,
                    CorrespondentId = senderUserId,
                    CorrespondentName = ""
                }, false);

            _unitOfWork.Transactions.Add(incoming);

            _unitOfWork.SaveChanges();
        }
    }
}
