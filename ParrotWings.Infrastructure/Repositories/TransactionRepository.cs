﻿using Microsoft.EntityFrameworkCore;
using ParrotWings.Infrastructure.Model;
using ParrotWings.Infrastructure.Repositories.Interface;
using System.Linq;

namespace ParrotWings.Infrastructure.Repositories
{
    public class TransactionRepository : RepositoryBase<TransactionEntity>, ITransactionRepository
    {
        public TransactionRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public IQueryable<TransactionEntity> GetUserTransaction(long userId)
        {
            return _appContext.Transaction.Where(t => t.UserId == userId).OrderBy(t => t.CreatedDate);
        }

        public decimal CalculateBalance(long userId)
        {
            return _appContext.Transaction.Where(t => t.UserId == userId).Sum(t => t.Amount);
        }
    }
}
