﻿using ParrotWings.Infrastructure.Repositories.Interface;

namespace ParrotWings.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        ITransactionRepository _transactions;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public ITransactionRepository Transactions
        {
            get
            {
                if (_transactions == null)
                    _transactions = new TransactionRepository(_context);

                return _transactions;
            }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

    }
}
