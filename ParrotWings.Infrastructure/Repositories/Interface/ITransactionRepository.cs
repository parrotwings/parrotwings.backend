﻿using ParrotWings.Infrastructure.Model;
using System.Linq;

namespace ParrotWings.Infrastructure.Repositories.Interface
{

    public interface ITransactionRepository : IRepository<TransactionEntity>
    {
        IQueryable<TransactionEntity> GetUserTransaction(long userId);
        decimal CalculateBalance(long userId);
    }
}
