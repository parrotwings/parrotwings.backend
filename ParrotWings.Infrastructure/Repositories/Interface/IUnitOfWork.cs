﻿namespace ParrotWings.Infrastructure.Repositories.Interface
{
    public interface IUnitOfWork
    {
        ITransactionRepository Transactions { get; }

        int SaveChanges();
    }
}
