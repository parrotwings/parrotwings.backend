﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ParrotWings.Infrastructure.Repositories
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();

            builder.UseSqlServer("Data Source=(local);Initial Catalog=ParrotWings;Persist Security Info=True;User ID=virto;Password=virto;Connect Timeout=30");

            return new ApplicationDbContext(builder.Options);
        }
    }
}
