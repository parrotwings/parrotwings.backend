﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ParrotWings.Infrastructure.Model;
using ParrotWings.Infrastructure.Security;

namespace ParrotWings.Infrastructure.Repositories
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
    {
        public DbSet<TransactionEntity> Transaction { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionEntity>().ToTable("Transaction").HasKey(x => x.Id);
            modelBuilder.Entity<TransactionEntity>().Property(x => x.CreatedDate).HasMaxLength(64);
            modelBuilder.Entity<TransactionEntity>().HasIndex(x => x.UserId)
                .IsUnique(false)
                .HasName("IX_UserId"); ;

            base.OnModelCreating(modelBuilder);
        }
    }
}
