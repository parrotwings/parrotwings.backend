﻿namespace ParrotWings.Infrastructure.Security
{
    public static class ClaimConstants
    {
        public const string Subject = "sub";

        public const string Permission = "permission";
    }

    public static class PropertyConstants
    {

        public const string FullName = "fullname";

        public const string JobTitle = "jobtitle";

        public const string Configuration = "configuration";
    }

    public static class ScopeConstants
    {
        public const string Roles = "roles";
    }
}
