﻿using Microsoft.AspNetCore.Identity;

namespace ParrotWings.Infrastructure.Security
{
    public class ApplicationUser : IdentityUser<long>
    {
        public string JobTitle { get; set; }
        public string FullName { get; set; }
        public string Configuration { get; set; }
        public bool IsEnabled { get; set; }
    }
}
