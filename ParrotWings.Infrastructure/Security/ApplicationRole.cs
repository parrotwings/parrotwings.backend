﻿using Microsoft.AspNetCore.Identity;

namespace ParrotWings.Infrastructure.Security
{
    public class ApplicationRole : IdentityRole<long>
    {
    }
}
