﻿namespace ParrotWings.Security.Model
{
    public class UserRegistration
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
